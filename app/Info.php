<?php
class Info {

    private function getCsv($csvFile)
    {
        $handle = fopen($csvFile, 'rb');
        if ($handle === false) {
            throw new Exception();
        }
        while (feof($handle) === false) {
            yield fgetcsv($handle);
        }
        fclose($handle);
    }

    public function parseCsv($csvFile): array
    {
        $array = [];
        foreach ($this->getCsv($csvFile) as $row) {
            $array[] = [
                'category' => $row[0],
                'firstname' => $row[1],
                'lastname' => $row[2],
                'email' => $row[3],
                'gender' => $row[4],
                'birthDate' => $row[5],
            ];
        }
        return $array;
    }

    public function prepareData($array): array
    {
        $parseData = [];
        foreach (array_chunk($array, 10) as $key => $value){
            $dbInfo = '';
            foreach ($value as $num => $item){
                $dbInfo .= '("'. $item['category'] . '","' . $item['firstname'].'","'.$item['lastname'].'","'.$item['email'].'","'.$item['gender'].'","'.$item['birthDate'] .'")';
                if ($num !== count($value) - 1) {
                    $dbInfo .= ',';
                }
            }
            $parseData[] = $dbInfo;
        }
        return $parseData;
    }

}