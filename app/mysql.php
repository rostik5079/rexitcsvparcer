<?php

class Mysql {

    protected $connect;

    public function __construct(Connector $connect)
    {
        $this->connect = $connect;
    }

    public function executeQuery($request)
    {
        return mysqli_query($this->connect->getConnect(),  $request );
    }

    public function insertMultipleData($multipleDataArray)
    {
        foreach ($multipleDataArray as $value){
            $this->executeQuery('INSERT INTO `csv`(`category`,`firstname`,`lastname`,`email`,`gender`,`birthDate`) VALUES '. $value . ';');
        }
    }
}