<?php


class Connector
{
    protected $hostname;
    protected $username;
    protected $password;
    protected $database ;
    protected $port;
    protected $conn;


    public function __construct
    (
        $hostname = '172.27.0.2',
        $username = 'rostik',
        $password = 'root',
        $database = 'rexit',
        $port = '3306'
    ) {
        $this->hostname = $hostname;
        $this->username = $username;
        $this->password = $password;
        $this->database = $database;
        $this->port = $port;

        $this->connect();
    }

    private function connect(): void
    {
        $this->conn = mysqli_connect($this->hostname, $this->username, $this->password, $this->database, $this->port);
    }

    public function getConnect(): mysqli
    {
        return $this->conn;
    }
}