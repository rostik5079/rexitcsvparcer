<?php

include 'app/Connector.php';
include 'app/mysql.php';
include 'app/Info.php';


$date = date('Y');
$limit = $_GET['count'] ?? 25;
$page = $_GET['page'] ?? 1;
$offset = $limit * ($page - 1);
$file = "data.csv";
$connect = new Connector();
$request = new Mysql($connect);
$mysql = new Info();

//$pullData = $mysql->prepareData($mysql->parseCsv($file));
//$request->insertMultipleData($pullData);      // загрузка данных в БД

$data = $request->executeQuery('SELECT * FROM `csv`');
$data = mysqli_fetch_all($data, MYSQLI_ASSOC);


$string = 'SELECT * FROM `csv` ';               //filters
if ($_GET['category']) {
    $string .= " WHERE category = '" . $_GET['category']."'";
}
if ($_GET['gender']){
    if (stripos($string, 'WHERE')){
        $string .= " AND `gender` = '".$_GET['gender']. "' ";
    } else {
        $string .= " WHERE `gender` = '".$_GET['gender']. "' ";
    }
}
if (is_numeric($_GET['age'])){
    if (stripos($string, 'WHERE')){
        $string .= " AND `birthDate` <= '" .($date - $_GET['age']).'-'.date('m-d'). "' ";
        $string .= " AND `birthDate` >= '".($date - $_GET['age'] - 1).'-'.date('m-d', strtotime(date('Y-m-d') . ' -1 day')). "' ";
    } else {
        $string .= " WHERE `birthDate` <= '" .($date - $_GET['age']).'-'.date('m-d'). "' ";
        $string .= " AND `birthDate` >= '".($date - $_GET['age'] - 1) .'-'.date('m-d', strtotime(date('Y-m-d') . ' -1 day')). "' ";
    }
}
if ($_GET['birthDate']){
    if (stripos($string, 'WHERE')){
        $string .= " AND `birthDate` = '".$_GET['birthDate']. "' ";
    } else {
        $string .= " WHERE `birthDate` = '".$_GET['birthDate']. "' ";
    }
}
if ($_GET['age_from'] && $_GET['age_to']){
    if (stripos($string, 'WHERE')){
        $string .= " AND `birthDate` <= '" .($date - $_GET['age_from'] - 1).'-'.date('m-d', strtotime(date('Y-m-d') . ' -1 day')). "' ";
        $string .= " AND `birthDate` >= '".($date - $_GET['age_to'] - 1).'-'.date('m-d', strtotime(date('Y-m-d') . ' -1 day')). "' ";
    } else {
        $string .= " WHERE `birthDate` <= '" .($date - $_GET['age_from'] - 1).'-'.date('m-d', strtotime(date('Y-m-d') . ' -1 day')). "' ";
        $string .= " AND `birthDate` >= '".($date - $_GET['age_to'] - 1) .'-'.date('m-d', strtotime(date('Y-m-d') . ' -1 day')). "' ";
    }
} //filters

$count = $request->executeQuery($string);
$count = mysqli_fetch_all($count, MYSQLI_ASSOC);
$len = floor(count($count) / $limit);

if ($_GET['export'] == 1) {
    $fp = fopen('export.csv', 'w');

    foreach ($count as $fields) {
        fputcsv($fp, $fields);
    }
    fclose($fp);
    header("Location: http://".$_SERVER['HTTP_HOST']."/export.csv");
}


$test = $request->executeQuery($string . " LIMIT " . $offset ."," . $limit);



?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<form action="index.php" method="get">
    <label>
        <select name="category">
                <option value="">Choose category</option>
                <?php
                foreach (array_unique(array_column($data, 'category')) as $value){
                    ?>
                    <option <?=$value==$_GET['category'] ? 'selected' : ''?> value="<?=$value?>"><?=$value?></option>
                    <?php
                }
                ?>
        </select>
    </label>
    <label>
           <select name="gender">
                <option value="">Choose gender</option>
                <?php
                foreach (array_unique(array_column($data, 'gender')) as $value){
                    ?>
                    <option <?=$value==$_GET['gender'] ? 'selected' : ''?> value="<?=$value?>"><?=$value?></option>
                    <?php
                }
                ?>
           </select>
    </label>
    <label>
        <input type="number" name="age" placeholder="Age" min="0" value="<?=$_GET['age']?>">
    </label>
    <label>
        <input type="date" name="birthDate" id="birthDate">
    </label>
    <label><input id="age_from" type="number" name="age_from" min="0" value="<?=$_GET['age_from']?>" placeholder="Age from"></label>
    <label><input id="age_to" type="number" name="age_to" min="0" value="<?=$_GET['age_to']?>" placeholder="Age to"></label>
    <select name="count" id="count">
        <option <?=10==$_GET['count'] ? 'selected' : ''?> value="10">10</option>
        <option <?=25==$_GET['count'] ? 'selected' : ''?> value="25">25</option>
        <option <?=50==$_GET['count'] ? 'selected' : ''?> value="50">50</option>
    </select>
    <p><input type="submit" value="Отправить"></p>
    <button type="submit" name="export" value="1">Export to csv</button>
</form>

    <table>
        <tr>
            <th>Category</th>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Email</th>
            <th>Gender</th>
            <th>Birth Date</th>
        </tr>

            <?php
            foreach ($test as $item) {
            ?>
        <tr>
            <td><?=$item['category']?></td>
            <td><?=$item['firstname']?></td>
            <td><?=$item['lastname']?></td>
            <td><?=$item['email']?></td>
            <td><?=$item['gender']?></td>
            <td><?=$item['birthDate']?></td>
        </tr>
            <?php }?>
    </table>

    <nav>
        <ul>
            <? for($i = 1; $i<= $len; $i++){
               $fun = ($_SERVER['REQUEST_URI'] == '/index.php' || $_SERVER['REQUEST_URI'] == '/') ? '?' : '&';?>
            <li><a href="<?=$_SERVER['REQUEST_URI'].$fun?>page=<?=$i?>"><?=$i?></a></li>
            <?}?>
        </ul>
    </nav>
</body>
</html>



21.05.2021




